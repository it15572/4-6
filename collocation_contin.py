#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 22 19:58:42 2018

@author: imogentaylor
"""
import scipy
import numpy as np
import math as mt
from scipy import integrate
from scipy.optimize import fsolve
import matplotlib
import matplotlib.pyplot as plt 

X0 = fsolve(shooting,x0, args = (t_val,k0,pars_d,func_duffing6))
max_0 = max_ampli(X0,k0,T,func_duffing6)              #finding max amplitude for k0
k1 = k0 + step_size
X1 = fsolve(shooting,X0, args = (t_val,k1,pars_d,func_duffing6))
max_1 = max_ampli(X1,k1,T,func_duffing6)    
x_init_d = [X0,X1]
k_init_d = [k0,k1]
max_steps_d = 325


def coeff(i,N):
    if i == 0 or i == N:
        c = 2
        return c
    else: 
        c = 1
        return c
def xval(j,N):
    return mt.cos((j*mt.pi)/N)

def Chebyshev(N):
    N = N
    C = np.zeros((N+1,N+1))
    for i in range(0,N+1):
        for j in range(0,N+1):
            if i == j and i == 0 and j == 0:
                C[0][0] = (2*(N**2)+1)/6
            elif i ==j and i == N and j == N:
                C[N][N] = -(2*(N**2)+1)/6
            elif i==j:
                C[j][j] = -xval(j,N)/(2*(1-(xval(j,N)**2)))
            else: 
                C[i][j] = (coeff(i,N)/coeff(j,N))*((-1)**(i+j))*(1/(xval(i,N) - xval(j,N)))
    return C


def contin_pseudo(contin_discret, func, x_init,k_init,pars,max_steps):
    x_con = np.array(x_init)
    c_con = np.array(k_init)
    max_amps = [max_0,max_1]              #find initial max amplitudes and put in here.
    for i in range(1,max_steps):
        x_secant = x_con[i] - x_con[i-1]                #calculate secants
        c_secant = c_con[i] - c_con[i-1]
        x_hat = x_con[i] + x_secant                     #calculate predicted values
        c_hat = c_con[i] + c_secant
        XC = np.append(x_hat,c_hat)
        new = fsolve(contin_discret, XC, args = (c_secant, c_hat, x_hat, x_secant,func,pars)) 
        x = new[:-1]
        c = new[-1]
        #adding corrected values to vectors for parameter and x-values
        c_con = np.append(c_con,c)
        x_con = np.vstack((x_con,x))  
        #finding max amplitude for corrected values of x and parameter
        max_amp = max_ampli(x,c,T,func)
         
        #adding max_amp for corrected parameter to vector of max amplitudes
        max_amps.append(max_amp)
    return (c_con,max_amps)



def pseudo_shooting(XC, c_secant, c_hat, x_hat, x_secant,func,pars):
    # figure out how to call x and c
    x = XC[:-1]
    c = XC[-1]
    a = np.append(x_secant,c_secant)
    d = x_hat - x
    e = c_hat - c
    f = np.append(d,e)
    pseudo_cond = np.dot(a,f)
    coll_cond = collocation(func_duffing5,)
    shoot_cond = shooting(x,t_val,c,pars,func)
    y = np.append(shoot_cond,pseudo_cond)
    return y

def func_duffing5(X,t,N,D,pars):
    tnew = t[0:-1]
    D = D[0:-1,:]
    x = np.reshape(X,(-1,2))
    z1 = np.dot(D,x[:,0]) - x[:,1][0:-1]
    z2 = np.dot(D,x[:,1]) - (Gamma)*elem_sin(tnew,N-1) - 2*xi*x[:,1][0:-1] - k*x[:,0][0:-1] - beta*(x[:,0][0:-1])**3
    z3 = x[:,0][0] - x[:,0][-1]
    z4 = x[:,1][0] - x[:,1][-1]
    z5 = np.append(z1,z2)
    z6 = np.array([z3,z4])
    return np.append(z5,z6)

def collocation(func, X1, t, N, D,pars):
    Xnew = fsolve(func,X1, args = (t,N,D,pars))
    return Xnew

N = 50
xi = 0.05
beta = 1
Gamma = 0.5
k = 0.1
pars = [xi,beta,Gamma,k]
t3 = tvals(N+1)
D3 = Chebyshev(N)
X3 = [1]*(2*N+2)
X0 = collocation(func_duffing5,X3,t3,N+1,D3,pars)
max0 = np.max(X0)
X1 = collocation(func_duffing5,X0,t3,N+1,D3,pars)
max1 = np.max(X1)