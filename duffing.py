# -*- coding: utf-8 -*-
"""
Created on Wed Oct 24 12:47:49 2018

@author: it15562
"""

import scipy
import numpy as np
import math as mt
from scipy import integrate
import matplotlib.pyplot as plt 


#defining t, first order system of odes that corresponds to duffing

t = np.linspace(0,300,1000)
def func(X,t):
    w= 0.5
    return [X[1], 0.2*mt.sin(w*t) - 2*0.05*X[1] - X[0] - (X[0])**3]

#using the integration function from scipy for different inital conditions

X0 = [1,0]
sol = integrate.odeint(func,X0,t)
X1 = [0,1]
sol1 = integrate.odeint(func,X1,t)
X2 = [3,4]
sol2 = integrate.odeint(func,X2,t)
X3 = [5,4]
sol3 = integrate.odeint(func,X3,t)

sol4 = integrate.odeint(func,[60,2],t)
sol5 = integrate.odeint(func,[0,0],t)
sol6 = integrate.odeint(func,[6,2],t)
sol7 = integrate.odeint(func,[8,2],t)  
sol8 = integrate.odeint(func,[10,0],t) 

#plotting different simulations to compare

plt.figure(figsize = (15,12))
fig, axes = plt.subplots(nrows=3, ncols=3, sharex=True)
titles = ['X0 = [1,0]', 'X0 = [0,1]', 'X0 = [3,4]', 'X0 = [5,4]', '','','','','']
x_vals = [sol[:,0], sol1[:,0],sol2[:,0],sol3[:,0],sol4[:,0],sol5[:,0],sol6[:,0],sol7[:,0],sol8[:,0] ]

for ax, title, x in zip(axes.flat, titles, x_vals):
    ax.plot(t, x)
    ax.set_title(title)
plt.plot(sol3[:,1],sol3[:,0])
fig.show()