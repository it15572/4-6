# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import numpy as np
import math as mt 
from scipy.optimize import fsolve
from scipy import integrate
import matplotlib.pyplot as plt
from numpy import *

"""Defining differentiation matrices """
#Finite Difference Matrix
def D_findiff2(N,h):
    A = np.zeros((N,N))
    for i in range(N):
        if i == 0:
            A[0,:][1] = 1
            A[0,:][N-1] = -1
        elif i == N-1:
            A[N-1,:][0] = 1
            A[N-1,:][N-2]= -1
        elif i != 0 and i!=N-1:  
            A[i,:][i+1] = 1
            A[i,:][i-1] = -1
    return A/(2*h)


#Functions needed to produce chebyshev matrix
def coeff(i,N):
    if i == 0 or i == N:
        c = 2
        return c
    else: 
        c = 1
        return c
def xval(j,N):
    return mt.cos((j*mt.pi)/N)

def Chebyshev(N):               #function for Chebyshev matrix
    N = N
    C = np.zeros((N+1,N+1))
    for i in range(0,N+1):
        for j in range(0,N+1):
            if i == j and i == 0 and j == 0:
                C[0][0] = (2*(N**2)+1)/6
            elif i ==j and i == N and j == N:
                C[N][N] = -(2*(N**2)+1)/6
            elif i==j:
                C[j][j] = -xval(j,N)/(2*(1-(xval(j,N)**2)))
            else: 
                C[i][j] = (coeff(i,N)/coeff(j,N))*((-1)**(i+j))*(1/(xval(i,N) - xval(j,N)))
    return C


#TESTS:
#from week5_chebtests.py import runtests_cheb
#runtests_cheb(Chebyshev)      
 
"""Writing a function to pass through fsolve in collocation function"""


#Function needed for zero problem - NOTE: ends up not being used.
def mat_vec(D,X,N):             #Calulates Dx
    result = np.zeros(N)
    for i in range(0,len(D)):
        for j in range(0,len(X)):
            result[j] += (D[i][j])*(X[j])
    return result

#Function needed for zero problem
def elem_sin(t,N):              #Calculates sin(mt.pi*t) for each t in t vector
    b = np.zeros(N)
    for i in range(0,len(b)):
        b[i] = mt.sin(mt.pi*t[i])
    return b
#Function to be passed to fsolve that produces x 
#that satisfies equation and boundary condition.     
def func(X,t,N,D,pars):
    tnew = t[0:-1]
    D = D[0:-1,:]
    z1 = X[0] - X[-1]                                           #boundary condition
    z2 = mat_vec(D,X[0:-1],N-1) - elem_sin(tnew,N-1) + X[0:-1]  #collocation
    z = np.append(z1,z2)
    return z
#New function that just uses np.dot instead of mat_vec - using this for collocation.
def func2(X,t,N,D,pars):
    tnew = t[0:-1]
    D = D[0:-1,:]
    z1 = X[0] - X[-1]               #boundary condition
    z2 = np.dot(D,X) - elem_sin(tnew,N-1) + X[0:-1] #collocation
    z = np.append(z1,z2)
    return z

""" Writing a new function for running collocation on 2D systems, e.g Duffing """
#Input 2N length array for X1 into fsolve, then reshape in function 
def func_duffing5(X,t,N,D,pars):
    tnew = t[0:-1]
    D = D[0:-1,:]
    x = np.reshape(X,(-1,2))                        #2D column vector
    z1 = np.dot(D,x[:,0]) - x[:,1][0:-1]            #collocation
    z2 = np.dot(D,x[:,1]) - (Gamma)*elem_sin(tnew,N-1) - 2*xi*x[:,1][0:-1] - k*x[:,0][0:-1] - beta*(x[:,0][0:-1])**3
    z3 = x[:,0][0] - x[:,0][-1]                     #boundary conditions
    z4 = x[:,1][0] - x[:,1][-1]
    z5 = np.append(z1,z2)
    z6 = np.array([z3,z4])
    return np.append(z5,z6)
    


""" Writing a function for collocation"""
def collocation(func, X1, t, N, D,pars):
    Xnew = fsolve(func,X1, args = (t,N,D,pars))
    return Xnew



""" Executing collocation using finite difference matrix """
#Inputs
N=50
t1 = np.linspace(-1,1,N)        #time points
h = 1                           #h for finite difference matrix
D1 = D_findiff2(N,h)            #calling finite difference matrix
pars = []                   
X1 = [0]*(N)                    #initial guess
X1 = np.asarray(X1)             #better to have in array form

#collocation solution:
xnew1 = collocation(func,X1,t1,N,D1,pars)

#analytic solution for our equation
x_actual1 = []
for i in range(len(t1)):
    g = 1/(1+mt.pi**2)*mt.sin(mt.pi*t1[i]) - mt.pi/(1+mt.pi**2)*mt.cos(mt.pi*t1[i])
    x_actual1.append(g)

#plotting collocation points against analytic solution
fig1, ax1 = plt.subplots()
ax1.plot(t1,xnew1, 'ro',label = 'Collocation solution')
ax1.plot(t1,x_actual1, 'b', label = 'Analytic Solution')
plt.legend(loc='upper left')

ax1.set(xlabel='time (t)', ylabel='x',
       title='Collocation for function dx/dt = sin(pi*t) - x, using finite difference')


#plt.savefig('finite_diff_sin.png')



""" Executing collocation using Chebyshev matrix """ 
#function for t values if using Chebyshev - outputs Chebyshev points
def tvals(N):
    t_vals = np.zeros(N)
    for i in range(N):
        t_vals[i] = xval(i,N-1)
    return t_vals

N = 50
t2 = tvals(N+1)                     #Chebyshev time points
D2 = Chebyshev(N)                   #calling Chebyshev matrix
X2 = [0]*(N+1)                      #initial guess
X2 = np.asarray(X2)                 #better to have in array form
pars = []

#collocation solution:
xnew2 = collocation(func2,X2, t2,N+1,D2,pars)

#analytic solution for our equation
x_actual2 = []
for i in range(len(t2)):
    g = 1/(1+mt.pi**2)*mt.sin(mt.pi*t2[i]) - mt.pi/(1+mt.pi**2)*mt.cos(mt.pi*t2[i])
    x_actual2.append(g)

#plotting collocation points against analytic solution
fig2, ax2 = plt.subplots()
ax2.plot(t2,xnew2, 'ro',label = 'Collocation solution')
ax2.plot(t2,x_actual2, 'b', label = 'Analytic Solution')
plt.legend(loc='upper center')
ax2.set(xlabel='time (t)', ylabel='x',
       title='Collocation for function dx/dt = sin(pi*t) - x, using Chebyshev')


#plt.savefig('chebyshev_sin4.png')



""" Executing collocation using Chebyshev matrix for Duffing equation."""
#inputs to collocation
N = 50
xi = 0.05               #defining parameters
beta = 1
Gamma = 0.5
k = 1
pars = [xi,beta,Gamma,k]
t3 = tvals(N+1)                     #Chebyshev time points
D3 = Chebyshev(N)                   #calling Chebyshev matrix
X3 = [1]*(2*N+2)                    #initial guess 
X3 = np.asarray(X3)

#collocation solution for coupled first order system
xnew3 = collocation(func_duffing5,X3,t3,N+1,D3,pars)
xnews = np.reshape(xnew3,(2,-1), order='F')         #reshaping into 2D column vector


#plotting collocation points 
fig4, ax4 = plt.subplots()
ax4.plot(t3,xnews[0,:],'ro', label = 'Collocation solution')
plt.legend(loc='upper right')
ax4.set(xlabel='time (t)',ylabel='x', title='Collocation for Duffing equation')


#plt.savefig('coll_duffing.png')


""""Comparing collocation solution to solution produce by odeint """
def funcduff(X,t,w):
    a = 0.05
    c = 0.5
    return [X[1], c*mt.sin(w*t) - 2*a*X[1] - X[0] - (X[0])**3]
t5= np.linspace(-1,1,len(t3))
a = integrate.odeint(funcduff,[-0.00212397,0.23095174],t3, args = (mt.pi,)) 
fig3, ax3 = plt.subplots()
ax3.plot(t3,xnews[0,:], 'ro',label = 'Collocation solution')
ax3.plot(t3,a[:,0], 'b', label = 'solution using odeint')
plt.legend(loc='upper right')
ax3.set(xlabel='time (t)', ylabel='x',
       title='Collocation vs. odeint for Duffing, using Chebyshev')


#plt.savefig('coll_duffing_odeint2.png')