# -*- coding: utf-8 -*-
"""
Created on Wed Nov 14 15:46:54 2018

@author: it15562
"""

def func_mspring(X,t,k,psi):
    return [X[1], math.sin(math.pi*t) - 2*psi*X[1] - k*X[0]]

#initial values:
X0 = integrate.odeint(func_mspring, [0,0], [0,T],args = (0.1,0.05))
X1 = integrate.odeint(func_mspring, [0,0], [0,T],args = (0.2,0.05))

def shooting(xs,t_val,k,psi):
    return (xs - integrate.odeint(func_mspring,xs,t_val, args = (k,psi)))[-1,:]
    
#mass spring damper
x0 = [0,0]
T=2
psi = 0.05
t_val = [0,T]


k = 0.1
X0 = fsolve(shooting,x0, args = (t_val,k,psi))
k = 0.2
X1 = fsolve(shooting,X0, args = (t_val,k,psi))


import math as mt
a = np.arange(-1,1,0.1)
b = np.zeros(len(a))

for i in range(len(a)):
    b[i] = math.sin(2*mt.pi*a[i])
N = len(a)
b_test = np.zeros(len(a))
for i in range(len(a)):
    b_test[i] = mt.cos(a[i])


from week5mod.py import x
from week5mod.py import Chebyshev
from week5mod.py import coeff
