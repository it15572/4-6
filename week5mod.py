# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import numpy as np
import math
import scipy 
from scipy.optimize import fsolve
from scipy import integrate
import matplotlib.pyplot as plt
import timeit 
from numpy import *


def D_findiff2(N,h):
    A = np.zeros((N,N))
    for i in range(N):
        if i == 0:
            A[0,:][1] = 1
            A[0,:][N-1] = -1
        elif i == N-1:
            A[N-1,:][0] = 1
            A[N-1,:][N-2]= -1
        elif i != 0 and i!=N-1:  
            A[i,:][i+1] = 1
            A[i,:][i-1] = -1
    return A/(2*h)

def coeff(i,N):
    if i == 0 or i == N:
        c = 2
        return c
    else: 
        c = 1
        return c
def xval(j,N):
    return math.cos((j*math.pi)/N)

def Chebyshev(N):
    N = N
    C = np.zeros((N+1,N+1))
    for i in range(0,N+1):
        for j in range(0,N+1):
            if i == j and i == 0 and j == 0:
                C[0][0] = (2*(N**2)+1)/6
            elif i ==j and i == N and j == N:
                C[N][N] = -(2*(N**2)+1)/6
            elif i==j:
                C[j][j] = -xval(j,N)/(2*(1-(xval(j,N)**2)))
            else: 
                C[i][j] = (coeff(i,N)/coeff(j,N))*((-1)**(i+j))*(1/(xval(i,N) - xval(j,N)))
    return C

#from week5_chebtests.py import runtests_cheb
runtests_cheb(Chebyshev)      
#if __name__ == "__main__":
#    import sys
#    D_findiff2(int(sys.argv[0]))
#    

def mat_vec(D,X,N):
    result = np.zeros(N)
    for i in range(0,len(D)):
        for j in range(0,len(X)):
            result[j] += (D[i][j])*(X[j])
    return result
            
def elem_sin(t,N):
    b = np.zeros(N)
    for i in range(0,len(b)):
        b[i] = math.sin(math.pi*t[i])
    return b
        
def func(X,t,N,D):
    tnew = t[0:-1]
    D = D[0:-1,:]
    z1 = X[0] - X[-1]
    z2 = mat_vec(D,X[0:-1],N-1) - elem_sin(tnew,N-1) + X[0:-1]
    z = np.append(z1,z2)
    return z

def func2(X,t,N,D):
    tnew = t[0:-1]
    D = D[0:-1,:]
    z1 = X[0] - X[-1]
    z2 = np.dot(D,X) - elem_sin(tnew,N-1) + X[0:-1]
    z = np.append(z1,z2)
    return z

def collocation(func, t, N, D,X1):
    Xnew = fsolve(func,X1, args = (t,N,D))
    return Xnew
#write collocation function that inputs x instead of t and that takes away the last line of D.


N = 20
def tvals(N):
    t_vals = np.zeros(N)
    for i in range(N):
        t_vals[i] = xval(i,N-1)
    return t_vals
""" Using finite difference matrix """
#h=10
#D = D_findiff2(N,h)  
#""" Using Chebyshev matrix """ 

D = Chebyshev(N)

#def init(t):
#    x0 = []
#    for i in range(len(t)):
#        a = math.sin(t[i])
#        x0.append(a)
#    return x0
#        
#X0 = np.asarray(init(t))
t = tvals(N+1)
global X1
X1 = [0]*(N+1)
X1 = np.asarray(X1)
xnew = collocation(func2,t,N+1,D,X1)

##finite diff
#t = tvals(N)
#global X1
#X1 = [0]*(N)
#X1 = np.asarray(X1)
#xnew = collocation(func2,t,N,D,X1)







x_actual = []
for i in range(len(t)):
    g = 1/(1+pi**2)*sin(pi*t[i]) - pi/(1+pi**2)*cos(pi*t[i])
    x_actual.append(g)


#plotting function
fig, ax = plt.subplots()
ax.plot(t,xnew, 'ro', t, x_actual, 'b')

ax.set(xlabel='time (t)', ylabel='x',
       title='Collocation for function dx/dt = sin(pi*t) - x')


""" Running the test """
if linalg.norm(xnew - x_actual) < 1e-6:
    print("ODE test 1 passed")
else:
    print("ODE test 1 failed")






