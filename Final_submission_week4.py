#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 17:29:37 2018

@author: imogentaylor
"""

# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import numpy as np
import math as mt
from scipy import integrate
import matplotlib.pyplot as plt 
from scipy.optimize import fsolve

#defining t and first order system of odes that corresponds to duffing

#setting variables for function

t = np.arange(0,200,0.02)
def func(X,t,w):
    xi = 0.05
    Gamma = 0.2
    return [X[1], Gamma*mt.sin(w*t) - 2*xi*X[1] - X[0] - (X[0])**3]

"""section for plotting and attempting different values of omega and initial conditions """
#using the integration function from scipy for different inital conditions
#to simulate how Duffing equation behaves.

w = 1                                      #use w = 1 as an example
sol0 = integrate.odeint(func,[1,1],t, args = (w,))
sol1 = integrate.odeint(func,[0,0],t, args = (w,))
sol2 = integrate.odeint(func,[3,0],t, args = (w,))
sol3 = integrate.odeint(func,[5,4],t, args = (w,))
sol4 = integrate.odeint(func,[10,0],t, args = (w,))
sol5 = integrate.odeint(func,[13,-1],t, args = (w,))
sol6 = integrate.odeint(func,[20,20],t, args = (w,))
sol7 = integrate.odeint(func,[40,0],t, args = (w,))  
sol8 = integrate.odeint(func,[60,0],t, args = (w,)) 

#plotting different simulations to compare

fig, axes = plt.subplots(nrows=3, ncols=3, sharex= True, figsize = (12,15))
titles = ['X0 = [1,1]', 'X0 = [0,0]', 'X0 = [3,0]', 'X0 = [5,4]', 'X0 = [60,0]','[20,20]','[10,0]','[13,-1]','[40,0]']
x_vals = [sol0[:,0], sol1[:,0],sol2[:,0],sol3[:,0],sol4[:,0],sol5[:,0],sol6[:,0],sol7[:,0],sol8[:,0] ]

for ax, title, x in zip(axes.flat, titles, x_vals):
    ax.plot(t, x)
    ax.set_title(title)
fig.show()

#choosing X0 = [3,0] and observing behaviour at different values of omega.
X0 = [3,0]
sol_1 = integrate.odeint(func,X0,t, args = (0.5,))             #w = 0.5
sol_2 = integrate.odeint(func,X0,t, args = (0.75,))             #w = 0.75
sol_3 = integrate.odeint(func,X0,t, args = (1,))             #w = 1
sol_4 = integrate.odeint(func,X0,t, args = (1.25,))             #w = 1.25
sol_5 = integrate.odeint(func,X0,t, args = (1.5,))             #w = 1.5

#Plotting solutions - NOTE: if time permitted,
# would figure out how to only produce 5 plots
fig, axes = plt.subplots(nrows=3, ncols = 2, sharex = True, sharey = True, figsize = (15,13))
titles = ['w=0.5', 'w=0.75', 'w=1', 'w=1.25', 'w=1.5']
x_valsw = [sol_1[:,0],sol_2[:,0],sol_3[:,0],sol_4[:,0],sol_5[:,0]]

for ax, title, x in zip(axes.flat, titles, x_valsw):
    ax.plot(t,x)
    ax.set_title(title)
fig.show()

"""Continuing with omega and inital conditions chosen: X0 = [3,0], w = 1"""

def R(xs,T):
    Tinit = [0,T]
    sols = integrate.odeint(func,xs,Tinit, args = (w,))[-1,:] #returns values at t = T
    return xs - sols                                #returns boundary condition

w=1
guess1 = 1
guess2 = 0
T = 2*mt.pi/w       #finding the exact period - could have found this approximately from the plot.
(solu1, solu2) = fsolve(R, [guess1,guess2], args = (T,))  #could choose any guess


#integrating forward to find solution using corrected initial conditions.
solss = integrate.odeint(func,[solu1,solu2],t, args = (w,)) 

#Plotting shooting solution and solution found using 'odeint'.
fig, ax = plt.subplots()
ax.plot(t,solss[:,0], 'r',label = 'shooting')
ax.plot(t,sol_3[:,0], 'b', label = 'solution found using odeint')
plt.legend(loc='upper right')
ax.set(xlabel='time (t)', ylabel='x',
       title='Shooting')

#plt.savefig('duffing shooting2.png')
