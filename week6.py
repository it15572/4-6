# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import numpy as np
import math as mt
from scipy import integrate
from scipy.optimize import fsolve
import matplotlib.pyplot as plt 

"""Defining functions to use """
#cubic equation
def func_cubic(x,c):
    return x**3 - x + c

#mass spring damper equation
def func_ms(X,t,pars,k):
    return [X[1], mt.sin(mt.pi*t) - 2*xi*X[1] - k*X[0]]

#duffing equation
def func_duffing6(X,t,pars,k): 
    return [X[1], (Gamma)*mt.sin((mt.pi)*t) - 2*xi*X[1] - k*X[0] - beta*(X[0])**3]

#shooting 
def shooting(x,t_val,k,pars,func):
    return (x - integrate.odeint(func,x,t_val, args = (pars,k)))[-1,:]



""" Functions to be passed into fsolve that include continuation condition
    and discretisation(if any) """
     
#define natural continuation method    
def natural_param(x, c_secant, c_hat, x_hat, x_secant,func,pars):
    return func(x,c_hat) 

#define pseudo-arclength continuation function
def pseudo_arclth(XC, c_secant , c_hat, x_hat, x_secant,func,pars):
    [x,c] = XC
    a = np.append(x_secant,c_secant)
    d = x_hat - x
    e = c_hat - c
    f = np.append(d,e)
    pseudo_cond = np.dot(a,f)
    z = np.append(pseudo_cond,func(x,c))
    return z
#define a pseudo-arclength function that includes the shooting method condition as well:
def pseudo_shooting(XC, c_secant, c_hat, x_hat, x_secant,func,pars):
    # figure out how to call x and c
    x = XC[:-1]
    c = XC[-1]
    a = np.append(x_secant,c_secant)
    d = x_hat - x
    e = c_hat - c
    f = np.append(d,e)
    pseudo_cond = np.dot(a,f)
    shoot_cond = shooting(x,t_val,c,pars,func)
    y = np.append(shoot_cond,pseudo_cond)
    return y
#define a natural parameter function that includes the shooting method condition:
def natural_shooting(x, c_secant, c_hat, x_hat, x_secant,func,pars):
    shoot_cond = shooting(x,t_val,c_hat,pars,func) #corrects x_hat along the line c=c_hat
    return shoot_cond




"""Defining continuation functions that create vectors of corrected values """
#performs predictive and corrective steps in a loop, properties are calculated if necessary, e.g. max amplitude

#pseudo-arclength continuation on cubic equation
def contin_pseudo_cubic(contin_discret,func,x_init,k_init,pars,max_steps):
    x_con = np.array(x_init)
    c_con = np.array(k_init)
    for i in range(1,max_steps):
        x_secant = x_con[i] - x_con[i-1]                #calculate secants
        c_secant = c_con[i] - c_con[i-1]
        x_hat = x_con[i] + x_secant                     #calculate predicted values
        c_hat = c_con[i] + c_secant
        XC = np.append(x_hat,c_hat)
        new = fsolve(contin_discret, XC, args = (c_secant, c_hat, x_hat, x_secant,func,pars)) 
        x = new[:-1]
        c = new[-1]
        #adding corrected values to vectors for parameter and x-values
        c_con = np.append(c_con,c)
        x_con = np.append(x_con,x)
    return (c_con,x_con)   

#natural parameter continuation on cubic equation
def contin_nat_cubic(contin_discret,func,x_init,k_init,pars,max_steps):
    x_con = np.array(x_init)
    c_con = np.array(k_init)
    for i in range(1,max_steps):
        x_secant = x_con[i] - x_con[i-1]                #calculate secants
        c_secant = c_con[i] - c_con[i-1]
        x_hat = x_con[i] + x_secant                     #calculate predicted values
        c_hat = c_con[i] + c_secant
        xnew = fsolve(contin_discret, x_hat, args = (c_secant, c_hat, x_hat, x_secant,func,pars)) 
        #adding corrected values to vectors for parameter and x-values
        c_con = np.append(c_con,c_hat)
        x_con = np.append(x_con,xnew)
    return (c_con,x_con)   

#pseudo-arclength continuation and including shooting for second-order ODE systems
def contin_pseudo(contin_discret, func, x_init,k_init,pars,max_steps):
    x_con = np.array(x_init)
    c_con = np.array(k_init)
    max_amps = [max_0,max_1]              #find initial max amplitudes and put in here.
    for i in range(1,max_steps):
        x_secant = x_con[i] - x_con[i-1]                #calculate secants
        c_secant = c_con[i] - c_con[i-1]
        x_hat = x_con[i] + x_secant                     #calculate predicted values
        c_hat = c_con[i] + c_secant
        XC = np.append(x_hat,c_hat)
        new = fsolve(contin_discret, XC, args = (c_secant, c_hat, x_hat, x_secant,func,pars)) 
        x = new[:-1]
        c = new[-1]
        #adding corrected values to vectors for parameter and x-values
        c_con = np.append(c_con,c)
        x_con = np.vstack((x_con,x))  
        #finding max amplitude for corrected values of x and parameter
        max_amp = max_ampli(x,c,T,func)
         
        #adding max_amp for corrected parameter to vector of max amplitudes
        max_amps.append(max_amp)
    return (c_con,max_amps)
#naural parameter continuation
def contin_nat(contin_discret,func,x_init,k_init,pars,max_steps):
    x_con = np.array(x_init)
    c_con = np.array(k_init)
    max_amps = [max_0,max_1]
    for i in range(1,max_steps): 
        x_secant = x_con[i] - x_con[i-1]
        c_secant = c_con[i] - c_con[i-1]
        x_hat = x_con[i] + x_secant
        c_hat = c_con[i] + c_secant
        xnew = fsolve(contin_discret, x_hat, args = (c_secant, c_hat, x_hat, x_secant,func,pars)) 
        max_amp = max_ampli(xnew,c_hat,T,func)
        c_con = np.append(c_con,c_hat)
        x_con = np.vstack((x_con,xnew))
        max_amps.append(max_amp)  
    return (c_con,max_amps)

"""Functions for calculating properties of system once corrected initial values found """
#function for calculating the property max amplitude
def max_ampli(x0_corr,k_corr,T,func):
    t = np.linspace(0,T,1000)
    #input corrected initial condition and parameter value into integrator
    points = (integrate.odeint(func,x0_corr,t, args = (xi,k_corr)))[:,0]
    #plt.plot(t,points)                 #test to see what max amplitude looks like
    return np.max(points)




"""Stating parameter values and initial conditions and producing
   plots for each of the three equations, showing both natural parameter
   and pseudo-arclength continuation"""
   

#cubic equation parameters
k0 = -1                 #starting parameter value
step_size = 0.01        #step size
kmax = 1                #max parameter value - unused.
pars_c = []             #other parameters to define


#Two initial points that satisfy cubic equation:
x0 = fsolve(func_cubic,5, args = k0)
k1 = k0 + step_size
x1 = fsolve(func_cubic, x0, args = k1)
x_init_c = [x0,x1]
k_init_c = [k0,k1]
max_steps_c = 450       

#plots:
(q,r) = contin_pseudo_cubic(pseudo_arclth, func_cubic, x_init_c, k_init_c, pars_c,max_steps_c)
(s,t) = contin_nat_cubic(natural_param, func_cubic, x_init_c, k_init_c, pars_c, max_steps_c)
fig1, ax1 = plt.subplots()
ax1.plot(q,r, 'b',label = 'Pseudo-arclength')
ax1.plot(s,t, 'ro', label = 'Natural Parameter')
plt.legend(loc='lower right')
ax1.set(xlabel='c', ylabel='x',
       title='Comparing Continuation Methods for x**3 - x + c = 0')


#plt.savefig('contin_cubic.png')




#mass spring damper parameters
k0 = 0.1            #starting parameter value      
step_size = 0.1     #step size
kmax = 20           #max parameter value - unused.
T=2                 #period
xi = 0.05           #other parameter
t_val = [0,T]       #boundary condition to input to shooting
x0 = [0,0]          #initial guess to put into fsolve for first point
pars_ms = [xi]      #pars to pass to fsolve

#Two intial points that satisfy mass-spring damper equation and x(0) = x(T)
X0 = fsolve(shooting,x0, args = (t_val,k0,pars_ms,func_ms))
max_0 = max_ampli(X0,k0,T,func_ms)              #finding max amplitude for k0
k1 = k0 + step_size
X1 = fsolve(shooting,X0, args = (t_val,k1,pars_ms,func_ms))
max_1 = max_ampli(X1,k1,T,func_ms)              #finding max amplitude for k1
x_init_ms = [X0,X1]
k_init_ms = [k0,k1]
max_steps_ms = 250

#plots:
(g,h) = contin_pseudo(pseudo_shooting,func_ms,x_init_ms,k_init_ms,pars_ms,max_steps_ms)
(i,j) = contin_nat(natural_shooting,func_ms,x_init_ms,k_init_ms,pars_ms,max_steps_ms)
fig2, ax2 = plt.subplots()
ax2.plot(g,h, 'bo',label = 'Pseudo-arclength')
ax2.plot(i,j, 'r', label = 'Natural Parameter')
plt.legend(loc='upper right')
ax2.set(xlabel='k', ylabel='||x||',
       title='Comparing Continuation Methods for Mass-Spring-Damper')

#plt.savefig('contin_mass-spring3.png')



#Duffing equation parameters
"""NOTE: have commented out calculating the natural parameter continuation values
to get the desired ouput for this course"""
k0 = 0.1                #starting parameter value
step_size = 0.1         #step size
kmax = 20               #maximum parameter value - unused.
T=2                     #period
xi = 0.05               #other parameters
beta = 1
Gamma = 1
t_val = [0,T]           #boundary condition to input to shooting
x0 =[0,0]               #initial guess to put into fsolve for first point
pars_d = [xi,beta,Gamma] #pars to pass to fsolve

#Initial points:
X0 = fsolve(shooting,x0, args = (t_val,k0,pars_d,func_duffing6))
max_0 = max_ampli(X0,k0,T,func_duffing6)    #finding max amplitude for k0
k1 = k0 + step_size
X1 = fsolve(shooting,X0, args = (t_val,k1,pars_d,func_duffing6))
max_1 = max_ampli(X1,k1,T,func_duffing6)    #finding max amplitude for k1
x_init_d = [X0,X1]
k_init_d = [k0,k1]
max_steps_d = 400

#plots
(e,f) = contin_pseudo(pseudo_shooting,func_duffing6,x_init_d,k_init_d,pars_d,max_steps_d)
#(c,d) = contin_nat(natural_shooting,func_duffing6,x_init_d,k_init_d,pars_d,max_steps_d)
fig3, ax3 = plt.subplots()
ax3.plot(e,f, 'b',label = 'Pseudo-arclength')
#ax3.plot(c,d, 'ro', label = 'Natural Parameter')
plt.legend(loc='upper right')
ax3.set(xlabel='k', ylabel='||x||',
       title='Continuation Methods for Duffing')

#plt.savefig('contin_duffing_1.png')




"""Arbitrary ODE systems should in theory be executed in the same way as mass-
-spring-damper and Duffing equation if maximum amplitude is property of interest"""

